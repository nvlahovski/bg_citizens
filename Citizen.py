from data.regions import egn_regions
from datetime import datetime


class Person:

    def __init__(self, day, month, year, gender, count_num, valid):

        self.day = day
        self.month = month
        self.year = year
        self.gender = gender
        self.__count_num = count_num
        self.valid = valid
        self.__regs = egn_regions()
        self.city = self.region()
        self.born_count = self.get_count_per_day()
        self.name = None
        self.surname = None
        self.family = None
        self.identy = self.__set_identy()

    def __set_identy(self):
        if int(self.year) > 1999:
            month = int(self.month) + 40
        elif int(self.year) < 1900:
            month = int(self.month) + 20
        else:
            month = int(self.month)
        m = datetime.strptime(str(self.year), '%Y').strftime('%y')
        egn = f"{str(m).zfill(2)}{str(month).zfill(2)}{str(self.day).zfill(2)}{str(self.__count_num)}"
        return self.__calc_control_egn(egn)

    def __calc_control_egn(self, egn: str) -> str:
        p = [int(a) for a in str(egn)]
        s = p[0] * 2 + p[1] * 4 + p[2] * 8 + p[3] * 5 + p[4] * 10 + p[5] * 9 + p[6] * 7 + p[7] * 3 + p[8] * 6
        control = s % 11

        result = egn + str(control)

        return result

    def isvalid_egn(self, p: list) -> bool:
        control = p[-1]
        s = p[0] * 2 + p[1] * 4 + p[2] * 8 + p[3] * 5 + p[4] * 10 + p[5] * 9 + p[6] * 7 + p[7] * 3 + p[8] * 6
        if s % 11 == control:
            return True
        return False

    def isvalid_date(self, year, mm, dd):
        iscorrect = True
        try:
            d = datetime(year, mm, dd)
            if d > datetime.now():
                iscorrect = False
        except ValueError as v:
            iscorrect = False
        return iscorrect

    @classmethod
    def get(cls, egn: int) -> object:
        assert isinstance(egn, int)
        p = [int(a) for a in str(egn)]
        assert len(p) == 10, 'wrong count'

        if cls.isvalid_egn(cls, p):
            year = int(str(p[0]) + str(p[1]))
            month = int(str(p[2]) + str(p[3]))
            day = int(str(p[4]) + str(p[5]))
            gender = 'm' if p[8] % 2 == 0 else 'f'
            count = str(p[6]) + str(p[7]) + str(p[8])
            if int(month) > 40:
                month = int(month) - 40
                year = int(year) + 2000
            elif month > 20:
                month = int(month) - 20
                year = year + 1800
            else:
                year = int(year) + 1900
            if cls.isvalid_date(cls, int(year), int(month), int(day)):
                valid = True
                return cls(day, month, year, gender, count, valid)

    def region(self):
        city = None
        for name, vals in self.__regs.items():
            if vals[0] <= int(self.__count_num) <= vals[1]:
                city = name
        if not city:
            self.valid = False
        return city

    def get_count_per_day(self):
        r = self.__regs[self.city]
        cnt = int(self.__count_num) - int(r[0])
        if cnt > 1:
            if cnt % 2 == 0:
                return int(cnt / 2)
            else:
                return int((cnt + 1) / 2)
        else:
            return 1

    @classmethod
    def set(cls, date: str, sex: str, city=None, count=None):
        if not sex in ('m', 'f'):
            raise ValueError('Wrong sex parameter')
        try:
            day, month, year = date.split('/')
        except ValueError as e:
            try:
                day, month, year = date.split('-')
            except ValueError as r:
                raise Exception(r)

        regsd = egn_regions()

        if city:
            if not city in regsd.keys():
                raise Exception('Not city found')
            else:
                if count:
                    errors = []
                    count_city = None
                    for name, vals in regsd.items():
                        if vals[0] <= int(count) <= vals[1]:
                            count_city = name
                    if not city == count_city:
                        errors.append(f'Wrong count for {city}')
                    if sex == 'm' and not count % 2 == 0:
                        errors.append(f"Count don't match for sex Male ")
                    if sex == 'f' and count % 2 == 0:
                        errors.append(f"Count don't match for sex Female ")

                    if errors:
                        raise ValueError(str(errors))
        else:
            if count:
                for name, vals in regsd.items():
                    if vals[0] <= int(count) <= vals[1]:
                        city = name
                if not city:
                    raise ValueError('Wrong count format')
            else:
                raise ValueError('Missing City and Count')

        if cls.isvalid_date(cls, int(year), int(month), int(day)):
            return cls(day, month, year, sex, count, True)
        else:
            raise ValueError('Wrong date')

    def __str__(self):
        names = f"{self.name if self.name else ''} {self.surname if self.surname else ''} {self.family if self.family else ''} "
        citizen = f"Born in {str(self.day).zfill(2)}-{str(self.month).zfill(2)}-{self.year}"
        city = f"Born place is: {self.city}"
        g = 'Male' if self.gender == 'm' else 'Female'
        sex = f'Person is: {g}'
        cn = f'Previously born baby`s with same sex {self.born_count}'
        identy = f'Identy: {self.identy}'
        return f" {names} \n {citizen} \n {city}\n {sex}\n {cn}\n {identy}\n"

    def __repr__(self):

        return f'Person({self.year},{self.month},{self.day}, {self.gender}, "{self.city}", {self.born_count}, {self.identy}, {self.valid})'



