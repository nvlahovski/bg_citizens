from Citizen import Person

person = Person.get(8202226323)

print(repr(person))
print(person)

person1 = Person.set("22/02/1982", 'm', 'София - град', 632)

person1.name = "Nikolay"
person1.surname = 'Nikolaev'
person1.family = 'Vlahovski'
print(person1)

person2 = Person.set("24/04/2022", 'm', None, 648)
person2.name = "Ivan"
person2.surname = 'Petrov'
person2.family = 'Dimitrov'
print(person2)

test_person_2 = Person.get(int(person2.identy))
print(test_person_2)